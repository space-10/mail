<?php
namespace Space10\Mail;

return array(
    'doctrine' => require __DIR__.'/doctrine.config.php',
    'form_elements' => [
        'invokables' => [
            'Space10\Mail\Form\Element\MailTransportType' => 'Space10\Mail\Form\Element\MailTransportType',
        ],
        'aliases' => [
            'mailtransporttype' => 'Space10\Mail\Form\Element\MailTransportType',
        ]
    ],
    'controllers' => array(
        'invokables' => array(
            'Space10\Mail\Admin\MailQueueController' => 'Space10\Mail\Admin\Controller\MailQueueController'
        )
    ),

    'console'         => [
        'commands' => [
            'Space10\Mail\Console\MailQueueCommand',
            'Space10\Mail\Console\MailSendCommand'
        ],
    ],

    'router' => array(
        'routes' => array(
            'admin' => array(
                'child_routes' => array(
                    'mail_queue' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/mail_queue[/:action[/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+'
                            ),
                            'defaults' => array(
                                'controller' => 'Space10\Mail\Admin\MailQueueController',
                                'action' => 'index'
                            )
                        )
                    )
                )
            )
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'Mail\Transport' => 'Space10\Mail\Service\MailTransportFactory',
            'Mail\Queue' => 'Space10\Mail\Service\QueueServiceFactory'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view'
        )
    )
)
;
