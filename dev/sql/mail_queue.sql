CREATE TABLE `mail_queue` (
  `queue_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(120) NOT NULL DEFAULT '',
  `sender_mail` varchar(255) NOT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `reply_to_mail` varchar(255) DEFAULT NULL,
  `reply_to_name` varchar(255) DEFAULT NULL,
  `receipient_mail` varchar(255) NOT NULL,
  `receipient_name` varchar(255) DEFAULT NULL,
  `body_html` text,
  `body_text` text,
  `send_date` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `status_text` text,
  PRIMARY KEY (`queue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;