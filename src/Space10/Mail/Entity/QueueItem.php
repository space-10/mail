<?php
namespace Space10\Mail\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Short description for Space10\Mail\Entity$QueueItem
 *
 * Long description for Space10\Mail\Entity$QueueItem
 *
 * @ORM\Entity
 * @ORM\Table(name="mail_queue", indexes={@ORM\Index(name="IDX_mail_queue_subject", columns={"subject"}), @ORM\Index(name="IDX_mail_queue_receipient", columns={"receipient_address", "receipient_name"})})
 */
class QueueItem extends Mail
{

    /**
     * @ORM\Id
     * @ORM\Column(name="queue_id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false, length=255)
     *
     * @var string
     */
    protected $subject;

    /**
     * @ORM\Column(name="from_address", type="string", nullable=false, length=255)
     *
     * @var string
     */
    protected $fromAddress;

    /**
     * @ORM\Column(name="from_name", type="string", nullable=false, length=255)
     *
     * @var string
     */
    protected $fromName;

    /**
     * @ORM\Column(name="reply_to_address", type="string", nullable=false, length=255)
     *
     * @var string
     */
    protected $replyToAddress;

    /**
     * @ORM\Column(name="reply_to_name", type="string", nullable=false, length=255)
     *
     * @var string
     */
    protected $replyToName;

    /**
     * @ORM\Column(name="receipient_address", type="string", nullable=false, length=255)
     *
     * @var string
     */
    protected $receipientAddress;

    /**
     * @ORM\Column(name="receipient_name", type="string", nullable=false, length=255)
     *
     * @var string
     */
    protected $receipientName;

    /**
     * @ORM\Column(name="body_html", type="text", nullable=true)
     *
     * @var string
     */
    protected $bodyHtml;

    /**
     * @ORM\Column(name="body_text", type="text", nullable=false)
     *
     * @var string
     */
    protected $bodyText;

    /**
     * @ORM\Column(name="sent_date", type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $sentDate;

    /**
     * @ORM\Column(name="status", type="smallint", nullable=true)
     */
    protected $status;

    /**
     * @ORM\Column(name="status_text", type="string", nullable=true, length=255)
     */
    protected $statusText;


    public function getId()
    {

        return $this->id;
    }

    public function setId($id)
    {

        $this->id = $id;
        return $this;
    }

}
