<?php
namespace Space10\Mail\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * Short description for Space10\Mail\Entity$Mail
 *
 * Long description for Space10\Mail\Entity$Mail
 *
 */
class Mail
{

    const DEFAULT_ENCODING = 'UTF-8';

    const TEMPLATE_TYPE_HTML = 'html';

    const TEMPLATE_TYPE_TEXT = 'text';

    /**
     *
     * @var string
     */
    protected $subject;

    /**
     *
     * @var string
     */
    protected $fromAddress;

    /**
     *
     * @var string
     */
    protected $fromName;

    /**
     *
     * @var string
     */
    protected $replyToAddress;

    /**
     *
     * @var string
     */
    protected $replyToName;

    /**
     *
     * @var string
     */
    protected $receipientAddress;

    /**
     *
     * @var string
     */
    protected $receipientName;

    /**
     *
     * @var string
     */
    protected $bodyHtml;

    /**
     *
     * @var string
     */
    protected $bodyText;

    /**
     *
     * @var string
     */
    private $templateHtml;

    /**
     *
     * @var string
     */
    private $templateText;

    /**
     *
     * @return string
     */
    public function getSubject()
    {

        return $this->subject;
    }

    /**
     *
     * @param string $subject
     * @return \Space10\Mail\Entity\Mail
     */
    public function setSubject($subject)
    {

        $this->subject = $subject;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getFromAddress()
    {

        return $this->fromAddress;
    }

    /**
     *
     * @param string $fromAddress
     * @return \Space10\Mail\Entity\Mail
     */
    public function setFromAddress($fromAddress)
    {

        $this->fromAddress = $fromAddress;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getFromName()
    {

        return $this->fromName;
    }

    /**
     *
     * @param string $fromName
     * @return \Space10\Mail\Entity\Mail
     */
    public function setFromName($fromName)
    {

        $this->fromName = $fromName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getReplyToAddress()
    {

        return $this->replyToAddress;
    }

    /**
     *
     * @param string $replyToAddress
     * @return \Space10\Mail\Entity\Mail
     */
    public function setReplyToAddress($replyToAddress)
    {

        $this->replyToAddress = $replyToAddress;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getReplyToName()
    {

        return $this->replyToName;
    }

    /**
     *
     * @param string $replyToName
     * @return \Space10\Mail\Entity\Mail
     */
    public function setReplyToName($replyToName)
    {

        $this->replyToName = $replyToName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getReceipientAddress()
    {

        return $this->receipientAddress;
    }

    /**
     *
     * @param string $receipientAddress
     * @return \Space10\Mail\Entity\Mail
     */
    public function setReceipientAddress($receipientAddress)
    {

        $this->receipientAddress = $receipientAddress;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getReceipientName()
    {

        return $this->receipientName;
    }

    /**
     *
     * @param string $receipientName
     * @return \Space10\Mail\Entity\Mail
     */
    public function setReceipientName($receipientName)
    {

        $this->receipientName = $receipientName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getBodyHtml()
    {

        return $this->bodyHtml;
    }

    /**
     *
     * @param string $bodyHtml
     * @return \Space10\Mail\Entity\Mail
     */
    public function setBodyHtml($bodyHtml)
    {

        $this->bodyHtml = $bodyHtml;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getBodyText()
    {

        return $this->bodyText;
    }

    /**
     *
     * @param string $bodyText
     * @return \Space10\Mail\Entity\Mail
     */
    public function setBodyText($bodyText)
    {

        $this->bodyText = $bodyText;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getTemplateHtml()
    {

        return $this->templateHtml;
    }

    /**
     *
     * @param string $templateHtml
     * @return \Space10\Mail\Entity\Mail
     */
    public function setTemplateHtml($templateHtml)
    {

        $this->templateHtml = $templateHtml;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getTemplateText()
    {

        return $this->templateText;
    }

    /**
     *
     * @param string $templateText
     * @return \Space10\Mail\Entity\Mail
     */
    public function setTemplateText($templateText)
    {

        $this->templateText = $templateText;
        return $this;
    }

    /**
     * Set template by type. Default is 'html'
     *
     * @param string $template
     * @param string $type
     * @return \Space10\Mail\Entity\Mail
     */
    public function setTemplate($template, $type = self::TEMPLATE_TYPE_HTML)
    {

        if ($type === static::TEMPLATE_TYPE_HTML)
        {
            return $this->setTemplateHtml($template);
        }
        else if ($type === static::TEMPLATE_TYPE_TEXT)
        {
            return $this->setTemplateText($template);
        }
        return $this;
    }
}
