<?php
namespace Space10\Mail\Form\Element;

use Zend\Form\Element\Select;

class MailTransportType extends Select
{

    /**
     * Constructor
     *
     * @param string $name
     * @param array $options
     */
    public function __construct($name = null, array $options = [])
    {

        parent::__construct($name, $options);
        $this->setValueOptions(
            [
                'smtp' => __('SMTP (Simple Mail Transfer Protocol)'),
                'sendmail' => __('Sendmail'),
                'file' => __('File')
            ]);
    }
}

