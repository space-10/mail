<?php
namespace Space10\Mail\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MailSendCommand extends Command implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;

    protected function configure()
    {
        $this
        ->setName('mail:send')
        ->setDescription('A Test.')
        ;
        $this->addArgument('subject', InputArgument::REQUIRED, 'Mail subject');
        $this->addArgument('from', InputArgument::REQUIRED, 'From which address we should send from');
        $this->addArgument('to', InputArgument::REQUIRED, 'To which mail should the <message> been sent');
        $this->addArgument('message', InputArgument::REQUIRED, 'The mail message');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $mailService \Space10\Mail\Service\MailService */
        $mailService = $this->serviceLocator->get('MailService');

        $htmlTemplate = <<<TMPL
<html>
<head></head>
<body>
<h1>Mail from command-line</h1>
<div>%s</div>
<br />
<br />
<div><img src="http://www.urlaubspiraten.de/media/images/2013/12/urlaub-copy-1387117402-P5iz-facebook%%402x.jpg" style="width:200px" /></div>
</body>
</html>
TMPL;

        $subject = $input->getArgument('subject');
        $from = $input->getArgument('from');
        $to = $input->getArgument('to');
        $message = $input->getArgument('message');
        $htmlMessage = sprintf($htmlTemplate, $message);

        $mailService->send($from, $to, $subject, $htmlMessage);

    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
