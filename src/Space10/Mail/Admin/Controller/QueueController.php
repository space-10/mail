<?php
namespace Space10\Mail\Admin\Controller;

/**
 * Short description for Divecheck\Mail\Admin\Controller$QueueController
 * 
 * Long description for Divecheck\Mail\Admin\Controller$QueueController
 * 
 * @copyright  Copyright  (c) 2014 Sirrus Systems GmbH (http://www.sirrus-systems.de/)
 * @version $Id$
 * @since Class available since revision $Revision$
 */
class QueueController 
{
    public function indexAction()
    {
        
    }
    
    public function pauseAction()
    {
        
    }
    
    public function resumeAction()
    {
        
    }
    
    public function abortAction()
    {
        
    }
    
    public function requeueAction()
    {
        
    }
    
}
