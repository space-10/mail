<?php
namespace Space10\Mail\Service;

use Space10\Mail\Entity\Mail;
use Zend\Mail\Message;
use Zend\Mail\Transport\TransportInterface;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;

/**
 * Class MailService
 * @package Space10\Mail\Service
 */
class MailService
{
    protected $transport;

    public function __construct(TransportInterface $transport) {
        $this->transport = $transport;
    }

    /**
     * @param $fromAddress
     * @param $toAddress
     * @param $subject
     * @param $bodyHtml
     * @param null $bodyText
     * @param null $fromName
     * @param null $toName
     */
    public function send($fromAddress, $toAddress, $subject, $bodyHtml, $bodyText = null, $fromName = null, $toName = null)
    {
        $from = $this->getZendMailAddress($fromAddress, $fromName);
        $to = $this->getZendMailAddress($toAddress, $toName);

        $message = new Message();
        $message->setSubject($subject);
        $message->setEncoding(Mail::DEFAULT_ENCODING);
        $message->setFrom($from);
        $message->setTo($to);
        $body = new MimeMessage();

        if ($bodyHtml) {
            $html = new MimePart($bodyHtml);
            $html->type = "text/html";
            $body->addPart($html);
        }

        if ($bodyText) {
            $text = new MimePart($bodyText);
            $text->type = "text/plain";
            $body->addPart($text);
        }

        /*
         * TODO: implement attachments
         */

        $message->setBody($body);

        $this->transport->send($message);
    }

    protected function getMailMessage(Mail $mail)
    {

        $message = new Message();

        $from = $this->getZendMailAddress($mail->getFromAddress(), $mail->getFromName());
        $repylTo = $this->getZendMailAddress($mail->getReplyToAddress(), $mail->getReplyToName());
        $receipient = $this->getZendMailAddress($mail->getReceipientAddress(), $mail->getReceipientName());
        $bodyHtml = $mail->getBodyHtml();
        $bodyText = $mail->getBodyText();

        if (!$from) {
            throw new \RuntimeException('Origin address is missing!');
        }

        if (!$receipient) {
            throw new \RuntimeException('Receipient address is missing!');
        }

        if (!$bodyHtml && !$bodyText) {
            throw new \RuntimeException('Mail body is missing!');
        }

        $message->setSubject($mail->getSubject());
        $message->setEncoding(\Space10\Mail\Entity\Mail::DEFAULT_ENCODING);

        $message->setFrom($from);
        $message->setTo($receipient);
        if ($repylTo) {
            $message->setReplyTo($repylTo);
        }

        $body = new MimeMessage();
        if ($bodyHtml) {
            $html = new MimePart($bodyHtml);
            $html->type = "text/html";
            $body->addPart($html);
        }

        if ($bodyText) {
            $text = new MimePart($bodyText);
            $text->type = "text/plain";
            $body->addPart($text);
        }

        /*
         * TODO: implement attachments
         */

        $message->setBody($body);
        return $message;
    }

    /**
     *
     * @param string $mail
     * @param string $name
     * @return \Zend\Mail\Address|NULL
     */
    private function getZendMailAddress($mail, $name = null)
    {

        $ret = null;
        if (is_string($mail)) {
            $ret = new \Zend\Mail\Address($mail, $name);
        }
        return $ret;
    }

    /**
     *
     * @return NULL|string
     */
    protected function renderMailTemplates(\Space10\Mail\Entity\Mail $mail, array $vars = [])
    {

        $htmlTemplate = $mail->getTemplateHtml();
        $textTemplate = $mail->getTemplateText();
        if (!$htmlTemplate && !$textTemplate) {
            return $this;
        }

        $viewModel = new ViewModel($vars);

        if ($htmlTemplate) {
            $viewModel->setTemplate($htmlTemplate);
            $body = $this->renderView($viewModel);
            if ($body) {
                $mail->setBodyHtml($body);
            }
        }

        if ($textTemplate) {
            $viewModel->setTemplate($textTemplate);
            $body = $this->renderView($viewModel);
            if ($body) {
                $mail->setBodyText($body);
            }
        }

        return $this;
    }

    protected function renderView(ViewModel $view)
    {

        /* @var $renderer \Zend\View\Renderer\RendererInterface */
        $renderer = Application::app()->get('Zend\View\Renderer\RendererInterface');
        return $renderer->render($view);
    }

    protected function sendLegacy(Mail $mail)
    {

        try {
            $this->getTransport()->send($mail->getMailMessage());
        } catch (\Zend\Mail\Exception\RuntimeException $e) {
            $message = $e->getMessage();

            // save item to database
            $item = new Item();
            $item->setData($mail->getData()); // receive data from mail, if any
            $item->setStatusText($message);
            $item->setStatus(Item::STATUS_ERROR);
            $item->save();
        }
    }
}
