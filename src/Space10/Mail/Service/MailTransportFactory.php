<?php

namespace Space10\Mail\Service;

use Zend\Mail\Transport;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 *
 * Short description for Divecheck\Mail\Service$MailTransportFactory
 *
 * Long description for Divecheck\Mail\Service$MailTransportFactory
 *
 * @copyright  Copyright  (c) 2013 Sirrus Systems GmbH (http://www.sirrus-systems.de/)
 * @license http://www.sirrus-systems.de/products/divecheck/license Divecheck Proprietary License
 * @version $Id$
 * @since Class available since revision $Revision$
 */
class MailTransportFactory implements FactoryInterface
{
    const XML_MAIL_TRANSPORT = 'mail/general/transport';


    /* (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /* @var $systemSettings \Space10\SystemSettings\Config\Config */
        $systemSettings = $serviceLocator->get('SystemSettings');
        $transportMode = $systemSettings->getConfig(self::XML_MAIL_TRANSPORT);

        $options = $systemSettings->getConfigNode('mail/general/options/'.$transportMode);

        switch ($transportMode) {
            case 'sendmail':
                $transport = new Transport\Sendmail();
                break;
            case 'smtp':
                $smtpOptions = new Transport\SmtpOptions($options->asArray());
                $transport = new Transport\Smtp($smtpOptions);
                break;
            case 'Zend\Mail\Transport\File':
            case 'File':
            case 'file':
                $options = new Transport\FileOptions([
                    'path'     => sys_get_temp_dir(),
                    'callback' => function ($transport) {
                        return basename(tempnam(sys_get_temp_dir(), 'ZendMail_' . time() . '_' . mt_rand()));
                    }
                ]);
                $transport = new Transport\File($options);
                break;
            default:
                throw new \DomainException(sprintf(
                    'Unknown mail transport type provided ("%s") or type isn\'t implemented yet.',
                    $transportMode
                ));
        }
        return $transport;
    }
}
