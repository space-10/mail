<?php
namespace Space10\Mail;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Short description for Divecheck\Rewrite$Module
 *
 * Long description for Divecheck\Rewrite$Module
 *
 * @copyright Copyright (c) 2014 Sirrus Systems GmbH (http://www.sirrus-systems.de/)
 * @version $Id$
 * @since Class available since revision $Revision$
 */
class Module implements ConfigProviderInterface
{

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\ModuleManager\Feature\ConfigProviderInterface::getConfig()
     */
    public function getConfig()
    {

        return include __DIR__ . '/etc/module.config.php';
    }
}
